package ua.com.epam;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static ua.com.epam.config.ChromeDriverProperties.NAME_DRIVER;
import static ua.com.epam.config.ChromeDriverProperties.PATH_TO_CHROME_DRIVER;
import static ua.com.epam.config.GmailProperties.*;
import static ua.com.epam.config.PathElement.*;

@Log4j2
public class GmailMessageTest {
    private WebDriver driver;
    private Wait<WebDriver> fwait;

    @BeforeTest
    public void setUp() {
        System.setProperty(NAME_DRIVER, PATH_TO_CHROME_DRIVER);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(PATH_TO_GOOGLE_LOGIN);
        fwait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(25))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
    }

    @Test
    public void sendGmailMsg() {
        log.info("Setting login.");
        WebElement inputMail = driver.findElement(By.id(INPUT_MAIL_BY_ID));
        inputMail.sendKeys(LOGIN);

        WebElement submitLogin = driver.findElement(By.xpath(SUBMIT_LOGIN_XPATH));
        submitLogin.click();

        log.info("Setting password.");
        WebElement inputPassword = driver.findElement(By.name(INPUT_PASSWORD_BY_NAME));
        inputPassword.sendKeys(PASSWORD);

        WebElement submitPass = fwait.until(ExpectedConditions
                .elementToBeClickable(driver.findElement(By.xpath(SUBMIT_PASSWORD_XPATH))));
        submitPass.click();

        log.info("Click by button 'Write'.");
        WebElement write = fwait.until(ExpectedConditions
                .elementToBeClickable(driver.findElement(By.cssSelector(WRITE_CSS_BY_CLASS_NAME))));
        write.click();

        log.info("Setting recipient.");
        WebElement whom = driver.findElement(By.xpath(WHOM_XPATH));
        whom.sendKeys(RECIPIENT);

        log.info("Setting topic.");
        WebElement topic = driver.findElement(By.xpath(TOPIC_XPATH));
        topic.sendKeys(MSG_TOPIC);

        log.info("Setting letter.");
        String textMsg = getRandomNumber();
        WebElement message = fwait.until(ExpectedConditions
                .elementToBeClickable(driver.findElement(By.xpath(MESSAGE_XPATH))));
        message.sendKeys(textMsg);

        log.info("Sending letter.");
        WebElement send = driver.findElement(By.xpath(SEND_XPATH));
        send.click();
        fwait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(SENT_WINDOW_XPATH))));

        log.info("Navigate to 'sent letter' tab.");
        WebElement tabSend = driver.findElement(By.xpath(SEND_TAB_XPATH));
        tabSend.click();

        log.info("Opening first letter.");
        WebElement firstLetter = fwait.until(ExpectedConditions
                .elementToBeClickable(driver.findElement(By.xpath(FIRST_LETTER_XPATH))));
        firstLetter.click();

        log.info("Getting message from first letter.");
        WebElement sentMsg = driver.findElement(By.xpath(MSG_XPATH));

        assertEquals(sentMsg.getText(), textMsg, "The letter was not sent.");

    }

    public String getRandomNumber() {
        Random rd = new Random();
        return String.valueOf(rd.nextInt((9999999 - 1) + 1) + 1);
    }

//    @AfterTest
//    public void closeSite() {
//        driver.quit();
//    }
}
