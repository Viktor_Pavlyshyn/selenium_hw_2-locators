package ua.com.epam.config;

public interface PathElement {
    String INPUT_MAIL_BY_ID = "identifierId";
    String INPUT_PASSWORD_BY_NAME = "password";
    String WRITE_CSS_BY_CLASS_NAME = "div.T-I.T-I-KE.L3";
    String WHOM_XPATH = "//textarea[@name='to']";
    String TOPIC_XPATH = "//input[@class='aoT']";
    String MESSAGE_XPATH = "//div[@class='Am Al editable LW-avf tS-tW']";
    String SEND_XPATH = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']";
    String SEND_TAB_XPATH = "//div[@class='TN bzz aHS-bnu']";
    String FIRST_LETTER_XPATH = "//tr[1][ancestor::div[@class='ae4 UI']]";
    String SUBMIT_PASSWORD_XPATH = "//button[@jscontroller='soHxf'][ancestor::div[@class='qhFLie']]";
    String SUBMIT_LOGIN_XPATH = "//button[@jsname='LgbsSe']";
    String MSG_XPATH = "//div[@class='a3s aXjCH ']/div[@dir='ltr']";
    String SENT_WINDOW_XPATH = "//span[@class='bAq' and contains(text(), 'Лист надіслано.')]";
}
