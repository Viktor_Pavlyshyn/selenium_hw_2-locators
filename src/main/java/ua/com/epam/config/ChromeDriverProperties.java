package ua.com.epam.config;

public interface ChromeDriverProperties {
    String PATH_TO_CHROME_DRIVER = "src/main/resources/chromedriver.exe";
    String NAME_DRIVER = "webdriver.chrome.driver";
}
